from __future__ import print_function
__all__ = ['TransformLookup']

import rospy
import tf2_ros

# monkey patches tf2_ros to support transforming geometry_msgs stamped msgs
# otherwise raises tf2_ros.buffer_interface.TypeException
import tf2_geometry_msgs


class TransformLookup(object):

    def __init__(self):
        self._tf_buffer = tf2_ros.Buffer()
        self._tf_listener = tf2_ros.TransformListener(self._tf_buffer)
        self._timeout = rospy.Duration.from_sec(1.0)

    def __call__(self, parent_frame, frame):
        # The shipping box TF frames are published by logical cameras
        # or can be published by user-created TF broadcasters.
        # Ensure that the transform is available.
        try:

            return self._tf_buffer.lookup_transform(
                parent_frame, frame, rospy.Time(), self._timeout
            )
        except (
            tf2_ros.LookupException, tf2_ros.ConnectivityException,
            tf2_ros.ExtrapolationException
        ) as e:
            rospy.logerr(e)
            return None

    def transform(self, local_pose, new_frame):
        return self._tf_buffer.transform(local_pose, new_frame)
